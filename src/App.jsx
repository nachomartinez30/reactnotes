import React, { Component } from 'react'
import './index.css'
import Note from './Notes/Note'
import NoteForm from './Notes/NoteForm'
/* FIREBASE CONEXION */
import firebase from 'firebase'
import { DB_CONFIG } from "./config/config";
import "firebase/database";

export default class App extends Component {
    constructor(props) {
        super(props)

        this.state = {
            notes: [
                // { noteId: 1, contenido: 'nota 1' },
                // { noteId: 2, contenido: 'nota 2' }
            ]
        }

        /* esto es para requerir la conexion a la base de datos */
        this.app = firebase.initializeApp(DB_CONFIG);

        /* conecta con  la coleccion de datos especifica en la base de datos */
        this.db = this.app.database().ref().child('notas')


        /* se agrega para que no pierda el scope los componentes hijos */
        this.agregarNota = this.agregarNota.bind(this)
        this.removeNote = this.removeNote.bind(this)

    }

    componentDidMount() {
        //cargar los datos dentro del estado
        const { notes } = this.state;

        /* Evento cuando un ITEM DE HA AGREGADO */
        this.db.on('child_added', (snap) => {
            notes.push({
                noteId: snap.key,
                contenido: snap.val().contenido
            })
            this.setState({ notes });
        })

        /* Evento cuando un ITEM ES REMOVIDO */
        this.db.on('child_removed', (snap) => {

            for (let i = 0; i < notes.length; i++) {
                /* si el ID de la nota es igual al ID del arreflo*/
                if (snap.id == notes[i].id) {
                    /* toma el indice y lo elimina del arreglo */
                    notes.slice(i, 1)
                }
            }
            /* actualiza el estado */
            this.setState({ notes });
            console.log('eliminado!');
        })
    }

    agregarNota(texto) {
        /* OBTIENE EL ESTADO ACTUAL */

        /* SE ELIMINA POR QUE DIDMOUNT ACTUALIZA EL ESTADO CONFORME REGISTROS EXISTAN EN LA BASE DE DATOS*/

        /* let { notes } = this.state */
        /* INSERTA UN NUEVO REGISTRO AL ESTADO ACTUAL */
        /* notes.push({
            noteId: notes.length + 1,
            contenido: texto
        }) */
        // this.setState({ notes })

        /* RENDERIZA EL COMPONENTE PARA MOSTRARLO */
        this.db.push().set({ contenido: texto })
    }

    removeNote(noteID) {
        this.db.child(noteID).remove();
    }

    render() {
        return (
            <div>
                <div className='notesHeader'>
                    <h1>Hi men</h1>
                </div>
                <div className='notesBody'>
                    <ul>
                        {this.state.notes.map(data =>
                            <Note
                                key={data.noteID}
                                id={data.noteId}
                                contenido={data.contenido}
                                removeNote={this.removeNote}
                            />
                        )}

                    </ul>
                </div>

                <div className='notesFooter'>
                    <NoteForm
                        agregarNota={this.agregarNota}
                    />
                </div>
            </div>
        )
    }
}
