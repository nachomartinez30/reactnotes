import React from 'react'
import "./Note.css";

function removeNote(props) {
    const respuesta = window.confirm(`¿desea eliminar la nota?`);
    if (respuesta) {
        props.removeNote(props.id)
    }
}

const Note = (prop) => {
    return (
        <div className='nota'>

            <span onClick={() => removeNote(prop)}>&times;</span>
            <p key={prop.id}>
                {prop.contenido}
            </p>
        </div>
    )
}



export default Note
