import React, { Component } from 'react'
import "./NoteForm.css";



export default class NoteForm extends Component {

    addNota(texto) {
        this.props.agregarNota(texto.value)

        /* limpia el componente input */
        texto.value = ''
        texto.focus()

    }

    render() {
        return (
            <div>
                <div className="NoteForm">
                    <input
                        ref={(input) => { this.textInput = input }}
                        type='text'
                        placeholder='Escribe una nota...'></input>
                    <button
                        onClick={() => this.addNota(this.textInput)}
                    >Agregar</button>
                </div>
            </div>
        )
    }
}
